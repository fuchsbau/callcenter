package de.fuchspfoten.callcenter;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Updates tickets.yml versions.
 */
public class TicketDataVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current version of tickets.yml.
     */
    public static final int VERSION = 1;

    @Override
    public void accept(final Integer integer, final DataFile dataFile) {
        // Do nothing.
    }
}
