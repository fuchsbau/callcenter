package de.fuchspfoten.callcenter;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.StringSerializer;
import de.fuchspfoten.fuchslib.data.YAMLSerializable;
import de.fuchspfoten.fuchslib.util.TimeHelper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Represents a support ticket.
 */
@RequiredArgsConstructor
public class Ticket implements YAMLSerializable {

    /**
     * Registers used message formats.
     */
    public static void registerMessages() {
        Messenger.register("callcenter.notify.closedOwn");
        Messenger.register("callcenter.notify.closed");
        Messenger.register("callcenter.notify.commentedOwn");
        Messenger.register("callcenter.notify.commented");
        Messenger.register("callcenter.notify.claimedOwn");
        Messenger.register("callcenter.notify.claimed");
        Messenger.register("callcenter.notify.escalatedOwn");
        Messenger.register("callcenter.notify.escalated");

        Messenger.register("callcenter.claim.statusMessage");
        Messenger.register("callcenter.close.statusMessage");
        Messenger.register("callcenter.escalate.statusMessage");

        Messenger.register("callcenter.detail.header");
        Messenger.register("callcenter.detail.location");
        Messenger.register("callcenter.detail.assignee");
        Messenger.register("callcenter.detail.visibility");
        Messenger.register("callcenter.detail.status");
        Messenger.register("callcenter.detail.messageHeader");
        Messenger.register("callcenter.detail.message");
        Messenger.register("callcenter.detail.helpComment");
        Messenger.register("callcenter.detail.helpClose");
    }

    /**
     * Loads a ticket with the given key from the given section.
     *
     * @param key     The key under which the section is stored.
     * @param section The section.
     * @return The loaded ticket.
     */
    public static Ticket load(final String key, final ConfigurationSection section) {
        final Ticket ticket = new Ticket(Integer.parseInt(key), UUID.fromString(section.getString("owner")),
                StringSerializer.parseLocation(section.getString("location")));
        ticket.load(section);
        return ticket;
    }

    /**
     * Formats a location for display.
     *
     * @param location The location.
     * @return The formatted location.
     */
    private static String formatLocation(final Location location) {
        return location.getWorld().getName() + " @ " + location.getBlockX() + ',' + location.getBlockY() + ','
                + location.getBlockZ();
    }

    /**
     * The ID of the ticket.
     */
    private @Getter final int id;

    /**
     * The UUID of the player that opened this ticket.
     */
    private @Getter final UUID ownerId;

    /**
     * The location at which this ticket was created.
     */
    private final Location creationLocation;

    /**
     * The messages associated with the ticket.
     */
    private final List<TicketMessage> messages = new ArrayList<>();

    /**
     * The UUID of the player that is assigned to this ticket.
     */
    private @Getter UUID assigneeId;

    /**
     * The status of the ticket.
     */
    private @Getter TicketStatus status = TicketStatus.OPEN;

    /**
     * The visibility of this ticket.
     */
    private @Getter TicketVisibility visibility = TicketVisibility.BASIC;

    /**
     * Whether this ticket contains unsaved changes.
     */
    private boolean dirty = true;

    /**
     * Adds a message to this ticket.
     *
     * @param player  The sender of the message.
     * @param message The message to send.
     */
    public void addMessage(final Entity player, final String message) {
        dirty = true;
        messages.add(new TicketMessage(player.getUniqueId(), System.currentTimeMillis(), message));
    }

    /**
     * Returns whether or not this ticket is expired. A ticket expires once it has been uncommented for some time.
     *
     * @return {@code true} iff this ticket is expired.
     */
    public boolean isExpired() {
        // Empty tickets are automatically expired.
        if (messages.isEmpty()) {
            return true;
        }

        // Open tickets never expire.
        if (status == TicketStatus.OPEN) {
            return false;
        }

        // Determine the time the ticket has been uncommented.
        final TicketMessage lastMessage = messages.get(messages.size() - 1);
        final long uncommentedAge = System.currentTimeMillis() - lastMessage.getTimestamp();

        // Closed tickets expire after one month.
        if (status == TicketStatus.CLOSED) {
            return uncommentedAge >= 30 * 86400000L;
        }

        // Resolved tickets expire after seven days.
        if (status == TicketStatus.RESOLVED) {
            return uncommentedAge >= 7 * 86400000L;
        }

        return false;
    }

    /**
     * Returns whether the ticket is visible for the given player.
     *
     * @param who The player.
     * @return {@code true} iff the ticket is visible for the given player.
     */
    public boolean isVisibleFor(final Entity who) {
        if (ownerId.equals(who.getUniqueId())) {
            return true;
        }

        // Determine visibility by the ticket's visibility.
        switch (visibility) {
            case BASIC:
                return who.hasPermission("callcenter.handle.basic");
            case RESTRICTED:
                return who.hasPermission("callcenter.handle.restricted");
            case ADMIN:
                return who.hasPermission("callcenter.handle.admin");
        }

        return false;
    }

    /**
     * Returns whether the ticket is closeable by the given player. Owners may only close tickets on
     * {@link de.fuchspfoten.callcenter.TicketVisibility#BASIC} visibility.
     *
     * @param who The player.
     * @return {@code true} iff the ticket is closeable by the given player.
     */
    public boolean isCloseableBy(final Entity who) {
        // Determine visibility by the ticket's visibility.
        switch (visibility) {
            case BASIC:
                if (ownerId.equals(who.getUniqueId())) {
                    return true;
                }
                return who.hasPermission("callcenter.handle.basic");
            case RESTRICTED:
                return who.hasPermission("callcenter.handle.restricted");
            case ADMIN:
                return who.hasPermission("callcenter.handle.admin");
        }

        return false;
    }

    /**
     * Returns a synopsis of the ticket's initial message.
     *
     * @return The synopsis of the initial message.
     */
    public String getSynopsis() {
        if (messages.isEmpty()) {
            return null;
        }

        final String message = messages.get(0).getMessage();
        return message.length() > 25 ? message.substring(0, 22) + "..." : message;
    }

    /**
     * Claims the ticket via the given entity.
     *
     * @param entity The entity to claim the ticket via.
     */
    public void claim(final Entity entity) {
        addMessage(entity, Messenger.getFormat("callcenter.claim.statusMessage"));
        assigneeId = entity.getUniqueId();
        dirty = true;
        ticketChangeNotification("callcenter.notify.claimed", "", entity.getName());
    }

    /**
     * Claims the ticket via the given entity.
     *
     * @param entity The entity to claim the ticket via.
     */
    public void escalate(final Entity entity) {
        addMessage(entity, Messenger.getFormat("callcenter.escalate.statusMessage"));

        // Notify before visibility change.
        ticketChangeNotification("callcenter.notify.escalated", "", entity.getName());
        visibility = visibility == TicketVisibility.BASIC ? TicketVisibility.RESTRICTED : TicketVisibility.ADMIN;
        dirty = true;
    }

    /**
     * Closes the ticket with the given message via the given entity.
     *
     * @param entity  The entity to close the ticket via.
     * @param message The close message.
     */
    public void closeWithMessage(final Entity entity, final String message) {
        // Add a close message and a close status message, then close the ticket.
        addMessage(entity, message);
        addMessage(entity, Messenger.getFormat("callcenter.close.statusMessage"));
        status = TicketStatus.CLOSED;
        dirty = true;
        ticketChangeNotification("callcenter.notify.closed", message, entity.getName());
    }

    /**
     * Comments the ticket with the given message via the given entity.
     *
     * @param entity  The entity to comment the ticket via.
     * @param message The comment message.
     */
    public void commentWithMessage(final Entity entity, final String message) {
        // Add a comment message.
        addMessage(entity, message);
        ticketChangeNotification("callcenter.notify.commented", message, entity.getName());
    }

    /**
     * Shows ticket details to the given entity. If the entity is the owner of the ticket and the ticket is closed, the
     * ticket is marked as resolved.
     *
     * @param entity The entity to show the ticket to.
     */
    public void showDetailsTo(final Entity entity) {
        Messenger.send(entity, "callcenter.detail.header", id);

        // Display clickable location data.
        final BaseComponent completeLocation = new TextComponent();
        final String locationText = String.format(Messenger.getFormat("callcenter.detail.location"),
                formatLocation(creationLocation));
        final String command = "/goto " + creationLocation.getWorld().getName() + ' ' + creationLocation.getBlockX()
                + ' ' + creationLocation.getBlockY() + ' ' + creationLocation.getBlockZ();
        for (final BaseComponent leftSub : TextComponent.fromLegacyText(locationText)) {
            leftSub.setClickEvent(new ClickEvent(Action.RUN_COMMAND, command));
            completeLocation.addExtra(leftSub);
        }
        entity.spigot().sendMessage(completeLocation);

        // Display base data: creation location and assignee, if applicable.
        if (assigneeId != null) {
            final String assigneeName = Bukkit.getOfflinePlayer(assigneeId).getName();
            Messenger.send(entity, "callcenter.detail.assignee", assigneeName);
        }

        // Display the status and visibility settings.
        Messenger.send(entity, "callcenter.detail.visibility", Messenger.getFormat(visibility.getFormat()));
        Messenger.send(entity, "callcenter.detail.status", Messenger.getFormat(status.getFormat()));

        // Display the message history.
        Messenger.send(entity, "callcenter.detail.messageHeader");
        for (final TicketMessage message : messages) {
            final String authorName = Bukkit.getOfflinePlayer(message.getAuthor()).getName();
            final long timeDiff = System.currentTimeMillis() - message.getTimestamp();
            Messenger.send(entity, "callcenter.detail.message", authorName,
                    TimeHelper.formatMostSignificantTimeDifference(timeDiff), message.getMessage());
        }

        // Show help lines for available actions.
        if (status == TicketStatus.OPEN) {
            Messenger.send(entity, "callcenter.detail.helpComment", id);
            if (isCloseableBy(entity)) {
                Messenger.send(entity, "callcenter.detail.helpClose", id);
            }
        }

        Messenger.send(entity, "callcenter.detail.header", id);

        // If the ticket is closed and the owner has been listing the ticket, change the ticket to resolved.
        if (status == TicketStatus.CLOSED) {
            if (ownerId.equals(entity.getUniqueId())) {
                status = TicketStatus.RESOLVED;
                dirty = true;
            }
        }
    }

    @Override
    public void save(final ConfigurationSection parentSection) {
        if (!dirty) {
            return;
        }
        dirty = false;

        final ConfigurationSection section = parentSection.createSection(String.valueOf(id));
        section.set("owner", ownerId.toString());
        section.set("assignee", assigneeId == null ? null : assigneeId.toString());
        section.set("status", status.name());
        section.set("visibility", visibility.name());
        section.set("location", StringSerializer.toString(creationLocation));

        final ConfigurationSection messageSection = section.createSection("messages");
        int idCounter = 0;
        for (final TicketMessage message : messages) {
            message.save(messageSection.createSection(String.valueOf(idCounter)));
            idCounter++;
        }
    }

    @Override
    public void load(final ConfigurationSection section) {
        dirty = false;
        if (section.isString("assignee")) {
            assigneeId = UUID.fromString(section.getString("assignee"));
        }
        status = TicketStatus.valueOf(section.getString("status"));
        visibility = TicketVisibility.valueOf(section.getString("visibility"));

        final ConfigurationSection messageSection = section.getConfigurationSection("messages");
        for (final String key : messageSection.getKeys(false)) {
            messages.add(TicketMessage.loadMessage(messageSection.getConfigurationSection(key)));
        }
    }

    /**
     * Performs a notification of a ticket change.
     *
     * @param format  The format to use for the notification.
     * @param message An additional message that is supplied for the ticket owner.
     * @param author  The name of the author performing the change.
     */
    private void ticketChangeNotification(final String format, final String message, final String author) {
        // Notify the owner, if possible.
        final Player ownerPlayer = Bukkit.getPlayer(ownerId);
        if (ownerPlayer != null && ownerPlayer.isOnline()) {
            Messenger.send(ownerPlayer, format + "Own", id, message);
        }

        // Notify all online handlers.
        for (final Player online : Bukkit.getOnlinePlayers()) {
            if (online.hasPermission(visibility.getPermission())) {
                Messenger.send(online, format, id, author);
            }
        }
    }
}
