package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * A module that notifies ticket owners on join.
 */
public class JoinNotifyModule implements Listener {

    /**
     * Constructor.
     */
    public JoinNotifyModule() {
        Messenger.register("callcenter.notify.existOwn");
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (CallcenterPlugin.getSelf().getTicketManager().ownsOpenTickets(event.getPlayer())) {
            Messenger.send(event.getPlayer(), "callcenter.notify.existOwn");
        }
    }
}
