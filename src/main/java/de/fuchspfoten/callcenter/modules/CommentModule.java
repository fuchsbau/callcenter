package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /comment &lt;id&gt; &lt;message...&gt; comments a ticket.
 */
public class CommentModule implements CommandExecutor {

    /**
     * The parser for command arguments for /comment.
     */
    private final ArgumentParser argumentParserComment;

    /**
     * Constructor.
     */
    public CommentModule() {
        argumentParserComment = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Syntax check.
        args = argumentParserComment.parse(args);
        if (argumentParserComment.hasArgument('h') || args.length < 2) {
            Messenger.send(sender, "commandHelp", "Syntax: /comment <id> <message...>");
            argumentParserComment.showHelp(sender);
            return true;
        }

        // Determine the id.
        final int id;
        try {
            id = Integer.parseInt(args[0]);
            if (id <= 0) {
                return true;
            }
        } catch (final NumberFormatException ex) {
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            builder.append(args[i]).append(' ');
        }
        final String message = builder.toString().trim();

        // Comment the ticket via the ticket manager.
        CallcenterPlugin.getSelf().getTicketManager().commentTicket(player, id, message);

        return true;
    }
}
