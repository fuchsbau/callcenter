package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /check [id] inspects a ticket or all tickets.
 */
public class CheckModule implements CommandExecutor {

    /**
     * The parser for command arguments for /check.
     */
    private final ArgumentParser argumentParserCheck;

    /**
     * Constructor.
     */
    public CheckModule() {
        argumentParserCheck = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Syntax check.
        args = argumentParserCheck.parse(args);
        if (argumentParserCheck.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /check [id]");
            argumentParserCheck.showHelp(sender);
            return true;
        }

        // No args: show all visible tickets. Args: show the specified ticket, if any.
        if (args.length == 0) {
            CallcenterPlugin.getSelf().getTicketManager().showTickets(player);
        } else {
            // Obtain the specified ticket id (greater than zero).
            final int id;
            try {
                id = Integer.parseInt(args[0]);
                if (id <= 0) {
                    return true;
                }
            } catch (final NumberFormatException ex) {
                return true;
            }

            // Perform display logic within the ticket manager (viewing may modify the ticket).
            CallcenterPlugin.getSelf().getTicketManager().showTicket(player, id);
        }

        return true;
    }
}
