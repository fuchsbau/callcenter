package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * /ticket &lt;message&gt; creates a ticket.
 */
public class TicketModule implements CommandExecutor {

    /**
     * The parser for command arguments for /ticket.
     */
    private final ArgumentParser argumentParserTicket;

    /**
     * The set of UUIDs currently on cooldown.
     */
    private final Collection<UUID> onCooldown = new HashSet<>();

    /**
     * Constructor.
     */
    public TicketModule() {
        Messenger.register("callcenter.ticket.moreInfo");
        Messenger.register("callcenter.ticket.onCooldown");

        argumentParserTicket = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Syntax check.
        args = argumentParserTicket.parse(args);
        if (argumentParserTicket.hasArgument('h') || args.length < 1) {
            Messenger.send(sender, "commandHelp", "Syntax: /ticket <message...>");
            argumentParserTicket.showHelp(sender);
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (final String arg : args) {
            builder.append(arg).append(' ');
        }
        final String message = builder.toString().trim();

        // Ensure verbosity: >= 4 words, >= 10 characters.
        if (message.length() < 10 || args.length < 4) {
            Messenger.send(sender, "callcenter.ticket.moreInfo");
            return true;
        }

        // Check if a cooldown is running for the ticket sender.
        if (onCooldown.contains(player.getUniqueId())) {
            Messenger.send(sender, "callcenter.ticket.onCooldown");
            return true;
        }

        // Add a cooldown for the player.
        onCooldown.add(player.getUniqueId());
        Bukkit.getScheduler().scheduleSyncDelayedTask(CallcenterPlugin.getSelf(),
                () -> onCooldown.remove(player.getUniqueId()), 60 * 20L);

        // Create the ticket.
        CallcenterPlugin.getSelf().getTicketManager().createTicket(player, message);

        return true;
    }
}
