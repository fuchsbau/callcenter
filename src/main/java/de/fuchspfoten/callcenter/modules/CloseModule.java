package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /close &lt;id&gt; &lt;message...&gt; closes a ticket with a message.
 */
public class CloseModule implements CommandExecutor {

    /**
     * The parser for command arguments for /close.
     */
    private final ArgumentParser argumentParserClose;

    /**
     * Constructor.
     */
    public CloseModule() {
        argumentParserClose = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Syntax check.
        args = argumentParserClose.parse(args);
        if (argumentParserClose.hasArgument('h') || args.length < 2) {
            Messenger.send(sender, "commandHelp", "Syntax: /close <id> <message...>");
            argumentParserClose.showHelp(sender);
            return true;
        }

        // Determine the id.
        final int id;
        try {
            id = Integer.parseInt(args[0]);
            if (id <= 0) {
                return true;
            }
        } catch (final NumberFormatException ex) {
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            builder.append(args[i]).append(' ');
        }
        final String message = builder.toString().trim();

        // Close the ticket via the ticket manager.
        CallcenterPlugin.getSelf().getTicketManager().closeTicket(player, id, message);

        return true;
    }
}
