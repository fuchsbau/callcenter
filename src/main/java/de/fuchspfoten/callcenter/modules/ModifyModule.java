package de.fuchspfoten.callcenter.modules;

import de.fuchspfoten.callcenter.CallcenterPlugin;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /modify &lt;id&gt; modifies a ticket.
 */
public class ModifyModule implements CommandExecutor {

    /**
     * The parser for command arguments for /modify.
     */
    private final ArgumentParser argumentParserModify;

    /**
     * Constructor.
     */
    public ModifyModule() {
        argumentParserModify = new ArgumentParser();
        argumentParserModify.addSwitch('c', "Claim a ticket");
        argumentParserModify.addSwitch('e', "Escalate a ticket");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Syntax check.
        args = argumentParserModify.parse(args);
        if (argumentParserModify.hasArgument('h') || args.length < 1) {
            Messenger.send(sender, "commandHelp", "Syntax: /modify <id>");
            argumentParserModify.showHelp(sender);
            return true;
        }

        // Obtain the specified ticket id (greater than zero).
        final int id;
        try {
            id = Integer.parseInt(args[0]);
            if (id <= 0) {
                return true;
            }
        } catch (final NumberFormatException ex) {
            return true;
        }

        // Ticket claiming.
        if (argumentParserModify.hasArgument('c')) {
            CallcenterPlugin.getSelf().getTicketManager().claimTicket(player, id);
        }

        // Ticket escalation.
        if (argumentParserModify.hasArgument('e')) {
            CallcenterPlugin.getSelf().getTicketManager().escalateTicket(player, id);
        }

        return true;
    }
}
