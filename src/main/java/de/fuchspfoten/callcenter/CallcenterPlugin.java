package de.fuchspfoten.callcenter;

import de.fuchspfoten.callcenter.modules.CheckModule;
import de.fuchspfoten.callcenter.modules.CloseModule;
import de.fuchspfoten.callcenter.modules.CommentModule;
import de.fuchspfoten.callcenter.modules.JoinNotifyModule;
import de.fuchspfoten.callcenter.modules.ModifyModule;
import de.fuchspfoten.callcenter.modules.TicketModule;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.DataFile;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * The main plugin class.
 */
public class CallcenterPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static CallcenterPlugin self;

    /**
     * The ticket manager.
     */
    private @Getter TicketManager ticketManager;

    /**
     * The data file for storing tickets.
     */
    private DataFile ticketData;

    @Override
    public void onEnable() {
        self = this;

        // Register messages.
        Messenger.register("callcenter.status.open");
        Messenger.register("callcenter.status.closed");
        Messenger.register("callcenter.status.resolved");
        Messenger.register("callcenter.visibility.basic");
        Messenger.register("callcenter.visibility.restricted");
        Messenger.register("callcenter.visibility.admin");
        TicketManager.registerMessages();
        Ticket.registerMessages();

        // Create default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create ticket data file.
        ticketData = new DataFile(new File(getDataFolder(), "tickets.yml"), new TicketDataVersionUpdater());

        ticketManager = new TicketManager();
        ticketManager.load(ticketData.getStorage());

        // Register listener modules.
        getServer().getPluginManager().registerEvents(new JoinNotifyModule(), this);

        // Register command modules.
        getCommand("check").setExecutor(new CheckModule());
        getCommand("close").setExecutor(new CloseModule());
        getCommand("comment").setExecutor(new CommentModule());
        getCommand("modify").setExecutor(new ModifyModule());
        getCommand("ticket").setExecutor(new TicketModule());

        // Register scheduled tasks.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, this::saveTickets, 1200L, 1200L);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, ticketManager::notifyHandlers, 6000L,
                6000L);
    }

    @Override
    public void onDisable() {
        saveTickets();
    }

    /**
     * Saves tickets.
     */
    private void saveTickets() {
        ticketManager.save(ticketData.getStorage());
        ticketData.save(TicketDataVersionUpdater.VERSION);
    }
}
