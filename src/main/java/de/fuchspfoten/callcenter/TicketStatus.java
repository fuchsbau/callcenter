package de.fuchspfoten.callcenter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents different ticket statuses.
 */
@RequiredArgsConstructor
public enum TicketStatus {

    /**
     * An open ticket has not yet been answered.
     */
    OPEN("callcenter.status.open"),

    /**
     * A closed ticket has been answered but the owner was not yet notified.
     */
    CLOSED("callcenter.status.closed"),

    /**
     * A resolved ticket has had its owner notified after it was closed.
     */
    RESOLVED("callcenter.status.resolved");

    /**
     * The {@link de.fuchspfoten.fuchslib.Messenger} format for display.
     */
    private @Getter final String format;
}
