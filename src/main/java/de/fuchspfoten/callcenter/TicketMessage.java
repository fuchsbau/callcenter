package de.fuchspfoten.callcenter;

import de.fuchspfoten.fuchslib.data.YAMLSerializable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

import java.util.UUID;

/**
 * Represents a message associated with a ticket.
 */
@RequiredArgsConstructor
public class TicketMessage implements YAMLSerializable {

    /**
     * Loads a ticket message from the given section.
     *
     * @param section The section to load from.
     * @return The loaded message.
     */
    public static TicketMessage loadMessage(final ConfigurationSection section) {
        return new TicketMessage(UUID.fromString(section.getString("author")), section.getLong("time"),
                section.getString("message"));
    }

    /**
     * The author of the message.
     */
    private @Getter final UUID author;

    /**
     * The timestamp at which the message was created.
     */
    private @Getter final long timestamp;

    /**
     * The message that is contained.
     */
    private @Getter final String message;

    @Override
    public void save(final ConfigurationSection section) {
        section.set("author", author.toString());
        section.set("time", timestamp);
        section.set("message", message);
    }

    @Override
    public void load(final ConfigurationSection section) {
        // Nothing to do.
    }
}
