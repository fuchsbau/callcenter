package de.fuchspfoten.callcenter;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.YAMLSerializable;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Manages all existing tickets.
 */
public class TicketManager implements YAMLSerializable {

    /**
     * Registers used message formats.
     */
    public static void registerMessages() {
        Messenger.register("callcenter.notify.createdOwn");
        Messenger.register("callcenter.notify.created");
        Messenger.register("callcenter.notify.exist");

        Messenger.register("callcenter.list.header");
        Messenger.register("callcenter.list.line");
        Messenger.register("callcenter.list.lineClaimed");
        Messenger.register("callcenter.list.footer");

        Messenger.register("callcenter.claim.locked");
        Messenger.register("callcenter.close.locked");
        Messenger.register("callcenter.comment.locked");
        Messenger.register("callcenter.detail.locked");
        Messenger.register("callcenter.escalate.locked");
    }

    /**
     * A mapping that maps ticket ids to tickets.
     */
    private final List<Ticket> ticketList = new ArrayList<>();

    /**
     * Creates a ticket.
     *
     * @param entity  The sender of the ticket.
     * @param message The ticket message.
     */
    public void createTicket(final Entity entity, final String message) {
        final Ticket ticket = new Ticket(findFreeId(), entity.getUniqueId(), entity.getLocation());
        ticket.addMessage(entity, message);
        storeTicket(ticket);

        // Notify the sender.
        Messenger.send(entity, "callcenter.notify.createdOwn", ticket.getId(), message);

        // Notify all online handlers.
        for (final Player online : Bukkit.getOnlinePlayers()) {
            if (online.hasPermission(TicketVisibility.BASIC.getPermission())) {
                Messenger.send(online, "callcenter.notify.created", ticket.getId(), ticket.getSynopsis(),
                        entity.getName());
            }
        }
    }

    /**
     * Attempts to close a ticket.
     *
     * @param entity  The entity attempting to close the ticket.
     * @param id      The ID of the ticket.
     * @param message The message to add.
     */
    public void closeTicket(final Entity entity, final int id, final String message) {
        final Ticket ticket = ticketList.size() > id ? ticketList.get(id) : null;
        if (ticket == null || !ticket.isCloseableBy(entity) || ticket.getStatus() != TicketStatus.OPEN) {
            Messenger.send(entity, "callcenter.close.locked");
            return;
        }

        ticket.closeWithMessage(entity, message);
    }

    /**
     * Attempts to comment a ticket.
     *
     * @param entity  The entity attempting to comment the ticket.
     * @param id      The ID of the ticket.
     * @param message The message to add.
     */
    public void commentTicket(final Entity entity, final int id, final String message) {
        final Ticket ticket = ticketList.size() > id ? ticketList.get(id) : null;
        if (ticket == null || !ticket.isVisibleFor(entity) || ticket.getStatus() != TicketStatus.OPEN) {
            Messenger.send(entity, "callcenter.comment.locked");
            return;
        }

        ticket.commentWithMessage(entity, message);
    }

    /**
     * Attempts to claim a ticket.
     *
     * @param entity The entity attempting to claim the ticket.
     * @param id     The ID of the ticket.
     */
    public void claimTicket(final Entity entity, final int id) {
        final Ticket ticket = ticketList.size() > id ? ticketList.get(id) : null;
        if (ticket == null || !entity.hasPermission(ticket.getVisibility().getPermission())
                || ticket.getStatus() != TicketStatus.OPEN) {
            Messenger.send(entity, "callcenter.claim.locked");
            return;
        }

        ticket.claim(entity);
    }

    /**
     * Attempts to escalate a ticket.
     *
     * @param entity The entity attempting to escalate the ticket.
     * @param id     The ID of the ticket.
     */
    public void escalateTicket(final Entity entity, final int id) {
        final Ticket ticket = ticketList.size() > id ? ticketList.get(id) : null;
        if (ticket == null || !entity.hasPermission(ticket.getVisibility().getPermission())
                || ticket.getStatus() != TicketStatus.OPEN || ticket.getVisibility() == TicketVisibility.ADMIN) {
            Messenger.send(entity, "callcenter.escalate.locked");
            return;
        }

        ticket.escalate(entity);
    }

    /**
     * Notifies ticket handlers of open tickets.
     */
    public void notifyHandlers() {
        final Map<TicketVisibility, Integer> openTicketCounts = new EnumMap<>(TicketVisibility.class);
        for (final Ticket ticket : ticketList) {
            if (ticket != null && ticket.getStatus() == TicketStatus.OPEN) {
                openTicketCounts.putIfAbsent(ticket.getVisibility(), 0);
                openTicketCounts.computeIfPresent(ticket.getVisibility(), (v, i) -> i + 1);
            }
        }

        // Determine the different numbers of tickets.
        final int basicTickets = openTicketCounts.getOrDefault(TicketVisibility.BASIC, 0);
        final int restrictedTickets = basicTickets
                + openTicketCounts.getOrDefault(TicketVisibility.RESTRICTED, 0);
        final int adminTickets = restrictedTickets
                + openTicketCounts.getOrDefault(TicketVisibility.ADMIN, 0);

        // Notify all online handlers.
        for (final Player online : Bukkit.getOnlinePlayers()) {
            if (online.hasPermission(TicketVisibility.ADMIN.getPermission()) && adminTickets > 0) {
                Messenger.send(online, "callcenter.notify.exist", adminTickets);
            } else if (online.hasPermission(TicketVisibility.RESTRICTED.getPermission()) && restrictedTickets > 0) {
                Messenger.send(online, "callcenter.notify.exist", restrictedTickets);
            } else if (online.hasPermission(TicketVisibility.BASIC.getPermission()) && basicTickets > 0) {
                Messenger.send(online, "callcenter.notify.exist", basicTickets);
            }
        }
    }

    /**
     * Shows all visible tickets to the given entity.
     *
     * @param entity The entity.
     */
    public void showTickets(final Entity entity) {
        Messenger.send(entity, "callcenter.list.header");
        for (final Ticket ticket : ticketList) {
            // Hide tickets if they're not existing or invisible for the viewer.
            if (ticket == null || !ticket.isVisibleFor(entity)) {
                continue;
            }

            // Hide tickets if they're a) resolved b) closed and not owned by the viewer.
            final boolean isOwner = ticket.getOwnerId().equals(entity.getUniqueId());
            if (ticket.getStatus() == TicketStatus.RESOLVED
                    || (!isOwner && ticket.getStatus() == TicketStatus.CLOSED)) {
                continue;
            }

            final Optional<String> assigneeName = Optional.ofNullable(ticket.getAssigneeId())
                    .map(Bukkit::getOfflinePlayer)
                    .map(OfflinePlayer::getName);
            final String format = ticket.getAssigneeId() != null
                    ? "callcenter.list.lineClaimed" : "callcenter.list.line";
            final OfflinePlayer owner = Bukkit.getOfflinePlayer(ticket.getOwnerId());
            Messenger.send(entity, format, ticket.getId(), owner.getName(), ticket.getSynopsis(),
                    Messenger.getFormat(ticket.getVisibility().getFormat()), assigneeName.orElse(""));
        }
        Messenger.send(entity, "callcenter.list.footer");
    }

    /**
     * Shows a ticket to the given entity if possible.
     *
     * @param entity   The entity.
     * @param ticketId The ticket id.
     */
    public void showTicket(final Entity entity, final int ticketId) {
        final Ticket ticket = ticketList.size() > ticketId ? ticketList.get(ticketId) : null;
        if (ticket == null || !ticket.isVisibleFor(entity)) {
            Messenger.send(entity, "callcenter.detail.locked");
            return;
        }

        ticket.showDetailsTo(entity);
    }

    /**
     * Checks whether or not a entity owns unresolved tickets.
     *
     * @param entity The entity to check.
     * @return {@code true} iff the entity owns unresolved tickets.
     */
    public boolean ownsOpenTickets(final Entity entity) {
        return ticketList.stream()
                .filter(Objects::nonNull)
                .filter(x -> x.getOwnerId().equals(entity.getUniqueId()))
                .anyMatch(x -> x.getStatus() != TicketStatus.RESOLVED);
    }

    /**
     * Finds a usable id for a ticket.
     *
     * @return The found id.
     */
    private int findFreeId() {
        int id = 1;
        while (true) {
            final Ticket currentTicket = ticketList.size() <= id ? null : ticketList.get(id);
            if (currentTicket == null || currentTicket.isExpired()) {
                break;
            }
            id++;
        }
        return id;
    }

    /**
     * Stores a ticket in the ticket list.
     *
     * @param ticket The ticket to store.
     */
    private void storeTicket(final Ticket ticket) {
        while (ticket.getId() >= ticketList.size()) {
            ticketList.add(null);
        }
        ticketList.set(ticket.getId(), ticket);
    }

    @Override
    public void save(final ConfigurationSection targetSection) {
        final ConfigurationSection subSection = targetSection.isConfigurationSection("tickets")
                ? targetSection.getConfigurationSection("tickets") : targetSection.createSection("tickets");
        for (final Ticket ticket : ticketList) {
            if (ticket == null) {
                continue;
            }
            ticket.save(subSection);
        }
    }

    @Override
    public void load(final ConfigurationSection sourceSection) {
        final ConfigurationSection subSection = sourceSection.isConfigurationSection("tickets")
                ? sourceSection.getConfigurationSection("tickets") : sourceSection.createSection("tickets");
        for (final String key : subSection.getKeys(false)) {
            final Ticket ticket = Ticket.load(key, subSection.getConfigurationSection(key));
            storeTicket(ticket);
        }
    }
}
