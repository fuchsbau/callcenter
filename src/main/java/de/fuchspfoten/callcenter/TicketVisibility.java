package de.fuchspfoten.callcenter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents visibilities for tickets.
 */
@RequiredArgsConstructor
public enum TicketVisibility {

    /**
     * Basic visibility, everyone with ticket management power can see this ticket.
     */
    BASIC("callcenter.visibility.basic", "callcenter.handle.basic"),

    /**
     * Restricted visibility, for example tickets that can't be solved by first tier support.
     */
    RESTRICTED("callcenter.visibility.restricted", "callcenter.handle.restricted"),

    /**
     * Even more restricted visibility, for example high-priority tickets that are confidential.
     */
    ADMIN("callcenter.visibility.admin", "callcenter.handle.admin");

    /**
     * The format for the visibility.
     */
    private @Getter final String format;

    /**
     * The permission required for manipulating tickets of the given visibility.
     */
    private @Getter final String permission;
}
